from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm, TodoItemForm


def todo_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/todo_list.html", context)


# Create your views here.
def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    items = todo_list.items.all()
    context = {
        "todo_list": todo_list,
        "items": items,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        item_forms = [
            TodoItemForm(request.POST, instance=item)
            for item in todo_list.items.all()
        ]
        if form.is_valid() and all(
            item_form.is_valid() for item_form in item_forms
        ):
            todo_list = form.save()
            for item_form in item_forms:
                item_form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
        item_forms = [
            TodoItemForm(instance=item) for item in todo_list.items.all()
        ]
    context = {"form": form, "todo_list": todo_list, "item_forms": item_forms}
    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    context = {"todo_list": todo_list}
    return render(request, "todos/todo_list_delete.html", context)
