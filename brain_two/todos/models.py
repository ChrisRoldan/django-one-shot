from django.db import models


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    todo_list = models.ForeignKey(
        "TodoList", on_delete=models.CASCADE, related_name="items"
    )

    def __str__(self):
        return self.task


class TodoList(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
